# Castalia Change Log

All notable changes to this project will be documented in this file.

We use [SemVer](https://semver.org/) for versioning.

## 5.0.0 - 2020-07-04

From version 3.3.0.

### Added

- Add makefrags for porting Castalia to OMNeT++ 5.
- New installation manual.

### Changed
- Changed makefiles for porting Castalia to OMNeT++ 5.