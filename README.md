# Castalia

Castalia is a simulator for Wireless Sensor Networks (WSN), 
Body Area Networks (BAN) and generally networks of low-power embedded devices.
It is based on the [OMNeT++](https://omnetpp.org/) platform and can be used by researchers and developers 
who want to test their distributed algorithms and/or protocols in realistic wireless channel and radio models, 
with a realistic node behaviour especially relating to access of the radio. 

Castalia can also be used to evaluate different platform characteristics for specific applications, 
since it is highly parametric, and can simulate a wide range of platforms. 

## Castalia on OMNeT++ 5

This repository contains an experimental image of Castalia that runs on OMNeT++ 5.

## Older Castalia releases (on OMNeT++ 4) 

For obtaining the latest official version 3.3, refer the [official repository](https://github.com/boulis/Castalia).
Alternatively, you can use the [tag 3.3.0](https://gitlab.com/francescoracciatti/castalia/-/tags/3.3.0) for downloading it.

Note that the official version cannot run on OMNeT++ 5.

## Getting Started

### Installing

For installing Castalia, refer the [installation manual](https://gitlab.com/francescoracciatti/castalia/-/blob/master/doc/installation-manual.pdf).

### Running

For running simulations, please refer the [user manual](https://gitlab.com/francescoracciatti/castalia/-/blob/master/doc/user-manual.docx).

## Forum

Castalia has a dedicated [forum](https://groups.google.com/forum/#!forum/castalia-simulator).
Search for existing questions before posting your own.

## Built With

### Development environment

* macOS High Sierra 10.13.6
* [Python](https://www.python.org/downloads/) 2.7.17 
* [OMNeT++](https://www.omnetpp.org/) 5.6.2
* [CLion](https://www.jetbrains.com/clion/) 2020.2.1

### Test and Deployment environment

* [VirtualBox](https://www.virtualbox.org/wiki/Downloads) 6.1.10
* [Ubuntu](https://ubuntu.com/) 18.04
* [Python](https://www.python.org/downloads/) 2.7.17
* [OMNeT++](https://omnetpp.org/) 5.6.2

## Contributing

Please read our [contributing instructions](CONTRIBUTING.md) and our [code of conduct](CODE_OF_CONDUCT.md),
for details on the process of submitting requests to us.

## Versioning

We use [SemVer](https://semver.org/) for versioning. For the versions available, see the tags on this repository.

## Author

Francesco Racciatti 

## Acknowledgements

Castalia's original authors:
* Thanassis Boulis
* Yuriy Tselishchev
* Dimosthenis Pediaditakis

## License

This project is licensed under the APL - Academic Public License. See the [LICENSE](LICENSE) for details.
