
.PHONY: all clean cleanall makefiles makefiles-so makefiles-lib makefiles-exe checkmakefiles

all: checkmakefiles
	cd src && $(MAKE) 
	mv src/CastaliaBin .

clean: checkmakefiles
	cd src && $(MAKE) clean
	rm -f CastaliaBin

cleanall: checkmakefiles
	@cd src && $(MAKE) MODE=release clean
	@cd src && $(MAKE) MODE=debug clean
	@rm -f src/Makefile $(FEATURES_H)

MAKEMAKE_OPTIONS := -f --deep -o CastaliaBin -O out -I.

makefiles: makefiles-exe

makefiles-so: 
	cd src && opp_makemake --make-so $(MAKEMAKE_OPTIONS) 

makefiles-lib: 
	cd src && opp_makemake --make-lib $(MAKEMAKE_OPTIONS) 

makefiles-exe:
	cd src && opp_makemake $(MAKEMAKE_OPTIONS)

